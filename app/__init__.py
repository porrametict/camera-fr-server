from flask import Flask,send_from_directory
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

app = Flask(__name__)
CORS(app)


app.config["SQLALCHEMY_DATABASE_URI"] = 'sqlite:///app.db'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config['SECRET_KEY'] = 'test-secret-string'

db = SQLAlchemy(app)

@app.route('/')
def index ():
    return "It's work!"

@app.route('/static/image/<path:path>')
def get_static(path):
    return send_from_directory('uploads',path)

from .src.blueprints.reset_db import rdb_pb
from .src.blueprints.face import face_group_pb
from .src.blueprints.image import image_pb
from .src.blueprints.profile import  profile_pb
app.register_blueprint(rdb_pb)
app.register_blueprint(face_group_pb)
app.register_blueprint(image_pb)
app.register_blueprint(profile_pb)
