from flask.globals import g
from sqlalchemy.orm import query
from .. import db 
import datetime
from sqlalchemy import desc

class Profile(db.Model):
    __tablename__ = "profiles"
    id = db.Column(db.Integer,primary_key=True)
    given_name_th = db.Column(db.String(100))
    family_name_th = db.Column(db.String(100))
    job_code = db.Column(db.Integer)
    position_title_thai = db.Column(db.String(255))
    start_nstda_work_date = db.Column(db.Date)
    start_work_date  = db.Column(db.Date)
    division_short_name = db.Column(db.String(50))
    department_short_name  =db.Column(db.String(50))
    degree  = db.Column(db.String(100))
    major = db.Column(db.String(100))
    quit_date = db.Column(db.Date())
    department_name_thai = db.Column(db.String(255))
    devision_name_thai = db.Column(db.String(255))
    face_encoding =  db.relationship('FaceGroup',backref='profile',lazy=True,uselist=False)
    
    def __repr__(self) : 
        return '<ImageSource %r>' % self.id

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
    @property
    def serialize(self):
        return {
            "id" : self.id,
            "given_name_th" : self.given_name_th
        }
class ImageSource(db.Model) : 

    __tablename__ = "image_sources"
    id = db.Column(db.Integer,primary_key=True)
    image = db.Column(db.Text,nullable=False)
    faces = db.relationship('Face',backref="image_sources",lazy=True)
    timestamp = db.Column(db.DateTime,nullable=False,default=datetime.datetime.now)

    def __repr__(self) : 
        return '<ImageSource %r>' % self.id
    
    @property
    def serialize(self)  : 
        return { 
            'id' : self.id,
            'image' : self.image,
            'timestamp' : self.timestamp.strftime("%Y-%b-%d %I:%M %p")
        }
    
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
     
class FaceGroup(db.Model) :
    __tablename__ = 'face_groups'
    id = db.Column(db.Integer,primary_key=True)
    profile_id = db.Column(db.Integer,db.ForeignKey('profiles.id'),nullable=True)
    sample_image_id = db.Column(db.Integer,db.ForeignKey('image_sources.id'),nullable=True)
    sample_encoding = db.Column(db.PickleType,nullable=False)
    group_type =  db.Column(db.Integer) #  0 add by user  , 1  from clustering unknownface
    faces = db.relationship('Face',backref="face_groups",lazy=True)
    timestamp = db.Column(db.DateTime,nullable=False,default=datetime.datetime.now)
    version =  db.Column(db.Integer,default=1)
    

    def __repr__(self) :
        return '<Group %r>' % self.id

    @property
    def serialize(self) : 
        return {
            'id' : self.id,
            'sample_image_id' : self.sample_image_id,
            # 'sample_encoding' : list(self.sample_encoding) ,
            'sample_image' : self.get_sample_image(),
            'face_set' : self.get_faces(),
            'profile' : self.get_profile(),
            'version' : self.version,
        }
    def get_profile_name(self) : 
        profile = Profile.query.get(self.profile_id)
        return  profile.given_name_th
    def get_profile(self):
        profile = Profile.query.get(self.profile_id)
        return profile.serialize
    
    def get_info(self):
        return {
            'id' : self.id,
            # 'name' : self.name,
            'profile' : self.get_profile(), 
        }

    def get_sample_image (self) :
        image =  ImageSource.query.get(self.sample_image_id)
        return image.serialize
    def get_faces(self) :
        faces = Face.query.filter_by(group_id=self.id).order_by(desc(Face.timestamp)).limit(10).all()
        return [face.serialize for face in faces]


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def get_face_encodings(cls):
        face_groups = cls.query.all()
        face_encodings , face_ids = [],[]
        for face_group in face_groups :
            face_encodings.append(face_group.sample_encoding)
            face_ids.append(face_group.id)
        
        return  face_ids,face_encodings
    @classmethod 
    def get_unknown_face_encoding(cls):
        face_groups = cls.query.filter_by(group_type=1)
        face_encodings , face_ids = [],[]
        for face_group in face_groups :
            face_encodings.append(face_group.sample_encoding)
            face_ids.append(face_group.id)
        
        return  face_ids,face_encodings
class Face(db.Model):
    __tablename__ = "faces"
    id = db.Column(db.Integer,primary_key=True)
    encoding = db.Column(db.PickleType,nullable=False)
    image_id = db.Column(db.Integer,db.ForeignKey('image_sources.id'),nullable=False)
    group_id = db.Column(db.Integer,db.ForeignKey('face_groups.id'),nullable=True)
    grouping_status = db.Column(db.Boolean,default=False)
    approve = db.Column(db.Boolean,default=False)
    timestamp = db.Column(db.DateTime,nullable=False,default=datetime.datetime.now)

    def __repr__(self) :
        return '<Face %r>' % self.id

    @property
    def serialize(self) : 
        return {
            'id' : self.id,
            # 'encoding' : list(self.encoding),
            'image_id' : self.image_id,
            'image' : self.image_serialize,
            'group_id' : self.group_id,
            'grouping_status' : self.grouping_status,
            'group' : self.get_group_info(),
            'approve' : self.approve,
            'timestamp' : self.timestamp.strftime("%Y-%b-%d %I:%M %p")
        }
    
    @property 
    def image_serialize(self) : 
        image =  ImageSource.query.filter_by(id=self.image_id).first()
        return image.serialize

    def get_group_info(self) :
        if self.group_id :
            g = FaceGroup.query.get(self.group_id)
            return g.get_info()
        return None 
        
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

class FaceVersion(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    version =  db.Column(db.Integer,default=0)
    
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
    
    def update(self,new_version):
        self.version = new_version
        db.session.commit()

    @classmethod 
    def update_version(cls) :
        qq = cls.query.get(1)
        if qq :
            qq.version += 1 
        else :
            newnew = cls(version=1)
            db.session.add(newnew)
        db.session.commit()