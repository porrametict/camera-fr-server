from flask_restful import Api
from ..resources import face,face_group,face_group_version,face_approve,unknown_face
from flask import Blueprint

face_group_pb = Blueprint('face_group',__name__,url_prefix="/api/face")
face_group_api = Api(face_group_pb)

face_group_api.add_resource(face_group.FaceGroupListResource,"/face-group")
face_group_api.add_resource(face_group.FaceGroupResource,"/face-group/<int:id>")

face_group_api.add_resource(face.FaceListResource,"/face")
face_group_api.add_resource(face.FaceResource,"/face/<int:id>")

face_group_api.add_resource(unknown_face.UnknownFaceListResource,"/unknown-face")
face_group_api.add_resource(unknown_face.ClusteringUnknownFaceResource,"/clustering")

face_group_api.add_resource(face_group_version.FaceGroupEncodingResource,"/face-group-encoding")
face_group_api.add_resource(face_group_version.FaceGroupVersionEncodingResource,"/face-group-version")

face_group_api.add_resource(face_approve.FaceApproveResource,"/face-approve")
face_group_api.add_resource(face_approve.FaceListApproveResource,"/face-list-approve")