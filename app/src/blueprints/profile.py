from flask_restful import Api
from ..resources import profile
from flask import Blueprint

profile_pb = Blueprint('profile',__name__,url_prefix='/api/profile')
profile_api = Api(profile_pb)

profile_api.add_resource(profile.ProfileListResource,"")