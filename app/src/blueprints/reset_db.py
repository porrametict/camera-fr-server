from flask_restful import Api
from ..resources import reset_db
from flask import Blueprint

rdb_pb = Blueprint('reset_db',__name__,)
rdb_api = Api(rdb_pb)

rdb_api.add_resource(reset_db.ResetDB,"/reset-db")

