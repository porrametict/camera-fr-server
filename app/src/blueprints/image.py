from flask_restful import Api
from ..resources import image
from flask import Blueprint

image_pb = Blueprint('image',__name__,url_prefix="/api/image")
image_api = Api(image_pb)

image_api.add_resource(image.ImageListResource,"")