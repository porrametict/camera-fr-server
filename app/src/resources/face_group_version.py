from flask_restful import Resource,reqparse
from ..models import FaceGroup,FaceVersion


class FaceGroupVersionEncodingResource (Resource) :
    def get(self) :
        ver_obj  = FaceVersion.query.get(1)
        return {"version": ver_obj.version if ver_obj is not None  else 0}


face_encoding_reqparse = reqparse.RequestParser()
face_encoding_reqparse.add_argument(
    'face_group_encoding',type=list,required=True,location='json'
)


class FaceGroupEncodingResource(Resource):
    def get(self): 
        """
        Given current all face_encodings | use for first time like a git clone
        """
        fv_obj = FaceVersion.query.get(1)
        face_version  = fv_obj.version
        groups = FaceGroup.query.all()
        return  {
            "version" : face_version,
            "groups_encoding":[
                {
                    "id":group.id,
                    "sample_encoding": list(group.sample_encoding ),
                    "profile_id"  : group.profile_id ,
                    "profile_name" : group.get_profile_name(),
                    "version" : group.version
                } for group in groups 
                ]
            }
        
    def post (self): # pull
        """
        Given current face_encdings with flags (add,update,remove)  | use be like a git pull 
        """
        args =  face_encoding_reqparse.parse_args()
        edge_face_encodings = args['face_group_encoding'] 
        face_group_encodings = FaceGroup.query.all()
        
        face_edge_ids =  set( face['id'] for face in edge_face_encodings)
        face_cen_ids = set(face.id for face in face_group_encodings)


        fv_obj = FaceVersion.query.get(1)
        face_version  = fv_obj.version

        update_lists = []
        # check new and update
        for face_cen in face_group_encodings  :
            if face_cen.id not in face_edge_ids : # if not in edge -> new face 
                update_lists.append(
                    {
                        "id": face_cen.id,
                        "sample_encoding" : list(face_cen.sample_encoding),
                        "profile_id" : face_cen.profile_id,
                        "profile_name" : face_cen.get_profile_name(),
                        "version" : face_cen.version,
                        "update_type" : "new"
                    }
                )
                continue
            
            for face_edge in edge_face_encodings :
                if face_cen.id ==  face_edge['id'] :     
                    if face_cen.version != face_edge['version'] : # if edge version is not equal current verion -> update face 
                        update_lists.append({
                               "id": face_cen.id,
                                "sample_encoding" : list(face_cen.sample_encoding),
                                "profile_id" : face_cen.profile_id,
                                "profile_name" : face_cen.get_profile_name(),
                                "version" : face_cen.version,
                                "update_type" : "update"
                        })       
                    break 
        
        # check remove
        for face_edge in edge_face_encodings :
            if face_edge['id'] not in face_cen_ids  :
                update_lists.append({
                    "id":face_edge['id'],
                    "update_type" : "remove"
                })
        
        return {
            "version" : face_version,
            "groups_encoding" : update_lists
        }
