import uuid 
from ... import db
from sqlalchemy import desc
from flask_restful import Resource
from ..easy_func.clustering import clustering_faces
from ..models import Face,FaceGroup,Profile,FaceVersion


class UnknownFaceListResource(Resource) :
    def get(self) :
        # Query Face don't have group
        faces = Face.query.filter_by(grouping_status=False).order_by(desc(Face.timestamp)).all()
        return [face.serialize for face in faces]

class ClusteringUnknownFaceResource(Resource) : 
    def post (self) :
        # Query Face don't have group
        faces =  Face.query.filter_by(grouping_status=False).all()
        # get ids, encodings 
        ids,encodings = self.get_ids_and_encoding(faces=faces)
        if len(ids) :
            groups = clustering_faces(ids,encodings)
            self.save_to_new_face_group(groups)
            FaceVersion.update_version()
            return "success"
        return None
    
    def save_profile(self,name):
        new_profile  = Profile(given_name_th=str(name))
        new_profile.save_to_db()
        return new_profile.id

    def get_ids_and_encoding (self,faces) :
        """
        Return (list of ids , list of encodings) of  faces 
        """
        ids,encodings = [],[]
        for face in faces :
            ids.append(face.id)
            encodings.append(face.encoding)
        return  ids,encodings
    
    def get_face(self,face_id) :
        return Face.query.get(face_id)
        
    def save_to_new_face_group(self, groups) :
        """
        Return (list of encoding )
        """
        for key in groups :
            if key != "-1" :  # -1 is not a group  
                face_items = groups[key]
                face = self.get_face(face_items[0]['face_id']) # get face instace
                name = f"Unknown_{uuid.uuid4().hex}" # randome name group 
                profile_id = self.save_profile(name) # create new profile
                # create new face group 
                new_face_group = FaceGroup(
                    profile_id = profile_id,
                    sample_image_id=face.image_id,
                    sample_encoding = face.encoding,
                    group_type=1
                )
                new_face_group.save_to_db()
                # assign group to each face 
                for i in face_items  :
                    face = self.get_face(i['face_id'])
                    face.group_id = new_face_group.id
                    face.grouping_status = True
                    db.session.commit()