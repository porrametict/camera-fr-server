from ... import db
import numpy as np 
from flask import request
from sqlalchemy import desc
from datetime import datetime
from flask_restful import Resource,reqparse
from ..easy_func.image import encoding_image
from ..models import Face,FaceGroup,Profile,FaceVersion
from face_recognition_classic_blue import get_cluster_center

approve_face_parser = reqparse.RequestParser()
approve_face_parser.add_argument(
    "face_group_id",type=int,location="json",required=True
)
approve_face_parser.add_argument(
    "face_ids" ,type=list, location="json",required=True
)


class  FaceApproveResource(Resource):
    def post (self):
        args = approve_face_parser.parse_args()
        face_group_id = args['face_group_id']
        face_ids =  args['face_ids'] # face_selectings

        # pick a sample face_encoding of group and group object
        gse,face_group_obj = self.get_group_sample_encoding(face_group_id) 
        # pick face_encoding of  new approve face 
        face_objs = self.get_face_encodings(face_ids)
        # approve new faces
        self.update_faces(face_objs)

        # pick all approved faces encodings in group
        fed = self.get_faces_group_encoding(face_group_id) 
        # pick a sample image of groups , re-encoding for use in Calculate group'centerpoint
        fed +=  self.get_sample_image_encoding(face_group_id)
        fed.append(gse)
        ## update face group
        new_sample = get_cluster_center(fed) # Find  Cluster point
        face_group_obj.sample_encoding = new_sample
        face_group_obj.version += 1 
        
        db.session.commit()
        FaceVersion.update_version()


        return "sucess",200
    def get_sample_image_encoding(self,g_id) :
        """
        Return encoding of image_sample
        """
        fg_obj = FaceGroup.query.get(g_id)
        sample_image  = fg_obj.get_sample_image()
        image = sample_image['image']
        encodings = encoding_image(image) 
        if len(encodings) == 1 :  # make sure in image are contain only 1 face.
            return encodings
        else : # don't believe -> for image that contain 2 face or later.
            return []
        
                        
        
    def get_faces_group_encoding(self,g_id):
        """
        Return all face_encodings in group
        """         
        approve_faces = Face.query.filter_by(approve=True) # get all face _approved
        
        face_encodings = [face.encoding for face in approve_faces]
        return face_encodings
        
    def update_faces (self,face_objs) :
        for face in face_objs :
            face.approve = True 
            db.session.commit()
        
    def get_group_sample_encoding (self,face_group_id):
        """
        Return all face_encodings in group
        """
        fg_obj = FaceGroup.query.get(face_group_id)
        sample_encoding = np.array(fg_obj.sample_encoding)
        return sample_encoding,fg_obj

    def get_face_encodings (self,face_ids) :
        """
        Return a list of face instace,get by id
        """
        face_objs = []
        for face_id in face_ids :
            f_obj = Face.query.get(face_id)
           
            face_objs.append(
                f_obj
            )
        return face_objs

class FaceListApproveResource(Resource) :

    def get(self) :

        per_page = 50
        # grap params
        page = int( request.args.get("page",1))
        name = request.args.get("name")
        face_group_id = request.args.get("face_group_id")
        start_datetime = request.args.get("start_datetime")
        end_datetime = request.args.get("end_datetime")
      
        faces = Face.query.order_by(desc(Face.id))
        
        filter_unapprove = True 
        if name  is not None  : # filter with  profile name
            profile_subquery = db.session.query(Profile.id).filter(Profile.given_name_th.startswith(name))
            group_sub_query = db.session.query(FaceGroup.id).filter(FaceGroup.profile_id.in_(profile_subquery))
            faces = faces.filter(Face.group_id.in_(group_sub_query))

        # filter by range(start_datetime,end_datetime)
        if start_datetime is not None : # start_datetime
            start_obj = datetime.strptime(start_datetime, '%Y-%m-%dT%H:%M')
            faces = faces.filter(Face.timestamp >= start_obj)
        if end_datetime is not None : # end_datetime
            end_obj = datetime.strptime(end_datetime, '%Y-%m-%dT%H:%M')
            faces = faces.filter(Face.timestamp <= end_obj)
        
        if face_group_id is not None : # filter group-id
            faces = faces.filter(Face.group_id ==  face_group_id)
        if filter_unapprove : # filter for un approve image 
            faces = faces.filter(Face.approve == False)
        faces = faces.paginate(page,per_page,error_out=False)

        context = {
            "pages" : faces.pages,
            "page" : faces.page,
            "prev_page" : faces.prev_num,
            "next_page" : faces.next_num,
            "items"  :[i.serialize for i in faces.items]
        }
        return  context