from ... import db
from flask import request
from sqlalchemy import desc
from flask_restful import Resource,reqparse
from ..easy_func.auto_map import face_auto_map
from ..models import Face,FaceGroup,ImageSource,Profile,FaceVersion
from ..easy_func.image import base64ToImage,saveImage,encoding_face


face_image_reqparse = reqparse.RequestParser()

face_image_reqparse.add_argument(
    'name' , required=True,location = 'json'
)
face_image_reqparse.add_argument(
    'image',required=True,location='json'
)

class FaceGroupListResource(Resource) :
    def get(self) :

        per_page = 20 
        page = int(request.args.get('page',1))
        name = request.args.get("name")
        
        # Query 
        groups = FaceGroup.query.order_by(desc(FaceGroup.timestamp))        
        if name is not None :
            profile_subquery = db.session.query(Profile.id).filter(Profile.given_name_th.startswith(name))
            groups = groups.filter(FaceGroup.profile_id.in_(profile_subquery))
        groups = groups.paginate(page,per_page,error_out=False)

        context =  {
            "pages" : groups.pages,
            "page" : groups.page,
            "prev_page" : groups.prev_num,
            "next_page" : groups.next_num,
            "items"  : [i.serialize for i in groups.items]
        }
        return context


    def post (self) : 

        # grap data 
        args = face_image_reqparse.parse_args()
        image_string = args['image']
        face_name = args['name']

        # Save new face profile 
        image,unsave_file_name  = base64ToImage(image_string)
        image_id,encoding = self.save_face(image,unsave_file_name)
        profile_id= self.save_profile(face_name)
        
        # find new face match any exist group ? 
        match_group  =  face_auto_map(encoding)

        if match_group is not None :
            # update that group
            face_group =  FaceGroup.query.get(match_group)
            face_group.profile_id = profile_id
            face_group.sample_image_id = image_id
            face_group.sample_encoding =  encoding 
            face_group.group_type = 1 
            db.session.commit()
        else :
            # create new group
            face_group = FaceGroup(profile_id=profile_id,sample_image_id=image_id,sample_encoding=encoding,group_type=0)
            face_group.save_to_db()
        FaceVersion.update_version()
        return face_group.serialize
    
    def save_profile(self,name):
        new_profile  = Profile(given_name_th=name)
        new_profile.save_to_db()
        return new_profile.id

    def save_image(self,image,file_name) :
        saveImage(image,file_name)
        new_image = ImageSource(image=file_name)
        new_image.save_to_db()
        return new_image.id
    
    def save_face(self,image,filename) : 
        image_id = self.save_image(image,filename)
        encoding = encoding_face(filename)  
        return image_id,encoding

class FaceGroupResource(Resource) :
    def get(self,id) :
        group = FaceGroup.query.get(id)
        return group.serialize