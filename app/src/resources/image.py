import face_recognition
from flask_restful import Resource,reqparse
from ..models import Face,FaceGroup,ImageSource
from ..easy_func.image import base64ToImage,saveImage,encoding_image
from ... import db
import numpy as np 
import time
from ...config import TOLERANCE
from face_recognition_classic_blue import predict_face
image_reqparse = reqparse.RequestParser()

image_reqparse.add_argument(
    'image' ,required=True,location='json'
)

class ImageListResource(Resource) :
    def get(self):
        images =  ImageSource.query.all()
        return  [i.serialize for i in images]
    def post(self) :
        args = image_reqparse.parse_args()
        image_string = args['image']
        image,unsave_file_name  = base64ToImage(image_string)
        self.save_faces(image,unsave_file_name)
        return "post"
    
    def save_image(self,image, file_name):
        """
        save Image to Device  and insert image into database
        """
        saveImage(image,file_name)
        new_image = ImageSource(image=file_name)
        new_image.save_to_db()
        return new_image.id
    
    def save_faces(self,image,filename):
        """
        Save Image then Save face from image
        """
        image_id = self.save_image(image,filename)
        face_encodings = encoding_image(filename)
        if len(face_encodings) :
            known_face_ids,known_face_encodings = FaceGroup.get_face_encodings()
            """
            match face with exist group
            """
            for face_encoding in face_encodings :
                new_face = Face(encoding=face_encoding,image_id=image_id)
                match_group = predict_face(known_face_ids,known_face_encodings,face_encoding)
                if match_group  is not None :
                    new_face.group_id = match_group
                    new_face.grouping_status = True
                new_face.save_to_db()
        return "success"
