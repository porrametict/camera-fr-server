from flask_restful import Resource
from ... import db
import os

def delete_all_files_in_uploads () :
    dir = "app/uploads"
    for f in os.listdir(dir) : 
        os.remove(os.path.join(dir,f))

class ResetDB(Resource):
    def get(self) :
        db.drop_all()
        db.create_all()
        delete_all_files_in_uploads()
        return "Success"