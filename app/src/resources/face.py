from ... import db
import numpy as np 
from flask import request
from sqlalchemy import desc
from datetime import datetime
from flask_restful import Resource,reqparse
from ..models import Face,FaceGroup,ImageSource,Profile
from ..easy_func.image import base64ToImage,saveImage


face_reqparse = reqparse.RequestParser()
face_reqparse.add_argument(
    'image',required=True,location='json'
)
face_reqparse.add_argument(
    'face_encoding',required=True,location='json'
)
face_reqparse.add_argument(
    'face_group_id' ,required=False,location='json'
)

class FaceListResource(Resource) :

    def get(self) :
        
        per_page = 20
        # grap params
        page = int( request.args.get("page",1))
        name = request.args.get("name")
        face_group_id = request.args.get("face_group_id")
        start_datetime = request.args.get("start_datetime")
        end_datetime = request.args.get("end_datetime")
      
        faces = Face.query.order_by(desc(Face.id))
        
        if name  is not None  : # filter with  profile name
            profile_subquery = db.session.query(Profile.id).filter(Profile.given_name_th.startswith(name))
            group_sub_query = db.session.query(FaceGroup.id).filter(FaceGroup.profile_id.in_(profile_subquery))
            faces = faces.filter(Face.group_id.in_(group_sub_query))

        # filter by range(start_datetime,end_datetime)
        if start_datetime is not None : # start_datetime
            start_obj = datetime.strptime(start_datetime, '%Y-%m-%dT%H:%M')
            faces = faces.filter(Face.timestamp >= start_obj)
        if end_datetime is not None : # end_datetime
            end_obj = datetime.strptime(end_datetime, '%Y-%m-%dT%H:%M')
            faces = faces.filter(Face.timestamp <= end_obj)
        
        if face_group_id is not None : # filter group-id
            faces = faces.filter(Face.group_id ==  face_group_id)
        faces = faces.paginate(page,per_page,error_out=False)

        context = {
            "pages" : faces.pages,
            "page" : faces.page,
            "prev_page" : faces.prev_num,
            "next_page" : faces.next_num,
            "items"  :[i.serialize for i in faces.items]
        }
        return  context
    
    def post(self) :
        

        args = face_reqparse.parse_args()
        image_string =  args['image']
        face_group_id = args['face_group_id']    
        face_encoding  =  np.array(args['face_encoding']) 

        # Save image and insert new face
        image_id = self.save_image(image_string)
        new_face = Face(
            encoding=face_encoding,
            image_id = image_id,
        )
        if face_group_id :
            new_face.group_id = face_group_id
            new_face.grouping_status  = True
        
        new_face.save_to_db()

    def save_image(self,image_str):
        image,unsave_file_name = base64ToImage(image_str)
        saveImage(image,unsave_file_name)
        new_image = ImageSource(image=unsave_file_name)
        new_image.save_to_db()
        return new_image.id 

class FaceResource(Resource) :
    def get(self,id) :
        face = Face.query.get(id)
        return face.serialize
    




