import uuid
from flask_restful import Resource,reqparse
from ..models  import  FaceGroup, ImageSource,Profile,FaceVersion
from ... import db 
import base64
import pandas as pd 
from io import StringIO
from ..easy_func import  image as img_func
from ..easy_func.auto_map import face_auto_map


profile_reqparse = reqparse.RequestParser()
profile_reqparse.add_argument(
    'csv_data' , required=True, location="json" ,type=list
)
profile_reqparse.add_argument(
    "images" , required=True , location="json" ,type=list
)

class ProfileListResource(Resource) : 
    def get(self) :
        return "profile api"
    def post (self) : 
        args  = profile_reqparse.parse_args()
        csv_data =  args['csv_data']  
        images = args['images']
         
        self.create_profile(csv_data)
        self.save_images(images)
        
        return "success"
    
    def create_profile (self,b64_list):
        """
        Insert profile to database.
        """

        # convert string to bytes and read CSV 
        code =  bytes(b64_list)
        b64_string = code
        csv_string =  base64.b64decode( bytes(b64_string)).decode('utf-8') 
        df = pd.read_csv(StringIO(csv_string)) 

        # Map between csv column name  to sqlmodel attribute
        match_attributes = {
        "EMPLOYEEID" :  "id",
        "GIVENNAMETHAI" : "given_name_th" ,
        "FAMILYNAMETHAI" : "family_name_th",
        "JOBCODE" : "job_code",
        "POSITIONTITLETHAI" : "position_title_thai",
        "STARTNSTDAWORKDATE" : "start_nstda_work_date",
        "STARTWORKDATE" : "start_work_date",
        "DivisionShortName" : "division_short_name",
        "DepartmentShortName" : "department_short_name",
        "DEGREE" : "degree",
        "MAJOR" : "major",
        "QUITDATE" : "quit_date",
        "DEPARTMENTNAMETHAI" : "department_name_thai",
        "DIVISIONNAMETHAI" : "devision_name_thai"
        }
        
        df = df.rename(columns=match_attributes) # Rename Column 
        df.to_sql(name='profiles',con=db.engine,index=False,if_exists="append") # add  data to sql db.

 
    
    def save_images(self,images):
        """ 
        Insert Image to Database 
        
        """
        for image in images :
            image_name = int( image['file_name'].split(".")[0] )
            image_data  = image['base64']
            image_id,image_encoding =  self.save_image(image_data)

            # find  exist group
            match_group  =  face_auto_map(image_encoding)

            profile_id  = self.get_profile_id(image_name)
            
            if match_group is not None :
                # if match with exist group  -> update info
                face_group = FaceGroup.query.get(match_group)
                face_group.profile_id = profile_id
                face_group.sample_image_id = image_id
                face_group.sample_encoding =  image_encoding 
                face_group.group_type = 0 
                db.session.commit()
            else :
                # not match with any group -> Create new Group
                face_group = FaceGroup(profile_id=profile_id,sample_image_id=image_id,sample_encoding=image_encoding,group_type=0)
                face_group.save_to_db()
            FaceVersion.update_version()

    def get_profile_id (self,image_name) :
         profile = Profile.query.filter_by(id=image_name).first()
         return profile.id
    def save_image(self,image : list ) :
        """
        Save Image and insert into database
        """
        image  = bytes(image)
        image  =  base64.b64decode(image)
        file_name  = f"{uuid.uuid4()}.jpg"

        # Save Image to Device Storage
        img_func.saveImage(image,file_name)

        
        new_image = ImageSource(image=file_name)
        new_image.save_to_db()
        
        encoding = img_func.encoding_face(file_name)
        return new_image.id,encoding
        