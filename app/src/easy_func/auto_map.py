import  pandas as  pd 
import numpy as np 
from ..models import  FaceGroup
from ...config  import  TOLERANCE
import face_recognition 
from face_recognition_classic_blue import predict_face
def face_auto_map(face_encoding):

    """
    Return exist group_id or None
    """

    known_face_ids,known_face_encodings = FaceGroup.get_unknown_face_encoding()
    return predict_face(known_face_ids,known_face_encodings,face_encoding)
