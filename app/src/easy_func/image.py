import base64
import uuid
import os 
import face_recognition
from ...config import min_face_height_threshold,min_face_width_threshold
from face_recognition_classic_blue import decode_base64

currect_path = os.getcwd()
UPLOAD_DIRECTORY = os.path.join(currect_path,"app/uploads/")

def base64ToImage(imgstring : str) : 
    """
    Convert base64String to bytes
    :return: bytes of image , random filenames
    """
    imagedata = decode_base64(imgstring)
    filename = f'{uuid.uuid4()}.jpg'
    return imagedata,filename

def saveImage (image,file_name) : 
    """
    Save Image File 
    """
    file_path = os.path.join(UPLOAD_DIRECTORY,file_name)

    if not os.path.isdir(UPLOAD_DIRECTORY) : 
        os.mkdir(UPLOAD_DIRECTORY)
    with open(file_path,'wb') as f :
        f.write(image)

def encoding_face(file_name):
    """
    Return a encoding face of image
    """
    file_path = os.path.join(UPLOAD_DIRECTORY,file_name)
    loaded_image = face_recognition.load_image_file(file_path)
    face_encoding = face_recognition.face_encodings(loaded_image)[0]
    return face_encoding

def filter_face_locations (face_locations) :
    """
    Return face_encodings that have width more than 100 points  and heigth more than 100 points
    """
    min_width_threshold = min_face_width_threshold
    min_height_threshold = min_face_height_threshold
    new_locations  = []

    for  face_location  in  face_locations :
        (top, right, bottom, left) = face_location
        if  (abs(top - bottom) < min_height_threshold ) or (abs(left - right ) < min_width_threshold ) : 
            pass 
        else : 
            new_locations.append(face_location)
    return new_locations
    
 
def encoding_image(file_name):
    """
    Return a list of face_encodings
    """
    file_path = os.path.join(UPLOAD_DIRECTORY,file_name)
    loaded_image = face_recognition.load_image_file(file_path)
    face_locations = face_recognition.face_locations(loaded_image)
    face_locations = filter_face_locations(face_locations)
    face_encodings = face_recognition.face_encodings(loaded_image,face_locations)
    return face_encodings
