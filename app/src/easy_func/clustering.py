import pandas as pd 
from sklearn.cluster import DBSCAN ,KMeans
from face_recognition_classic_blue import clustering_faces as clustering_encoding_faces,get_cluster_center

def group_by_label(data,list_encoding,list_face_id) :
    """
    Return a dict of grouping
    """
    labels = {} 
    for face_index in range(len(data)) : 
        key  = f"{data['label'][face_index]}"
        if key not in labels : 
            labels[key]  = [
                {
                    'face_id' :list_face_id[face_index],
                    'encoding' : list_encoding[face_index]
                }
            ]
        else : 
            labels[key].append({
                    'face_id' :list_face_id[face_index],
                    'encoding' : list_encoding[face_index]
                })
    return labels  

def clustering_faces (face_ids,face_encodings) :
    df = clustering_encoding_faces(face_encodings)
    groups = group_by_label(df,face_encodings,face_ids)
    return groups 

def group_find_sample(face_items : list ) :
    """
    Return a center point of list face_encodings
    """
    group_face_encodings =  [ face_item['encoding'] for face_item in face_items]
    return get_cluster_center(group_face_encodings) 
