# Camera FR Server
### SetupProject
1. install dliib
https://gist.github.com/ageitgey/629d75c1baac34dfa5ca2a1928a7aeaf
2. clone project and create virtual environment
```
git clone https://gitlab.com/porrametict/camera-fr-server.git
cd  camera-fr-server
virtualenv venv
source  venv/bin/activate
```
3. install essential module 
```
pip install face_recognition
pip install git+https://gitlab.com/porrametict/face_recognition_classic_blue.git
pip install -r requirements.txt
```

### Run 
```
export FLASK_APP=app
flask run 
```

### Reference
[face_recognition](https://github.com/ageitgey/face_recognition/blob/master/README.md)
[face_regcognition_classic blue](https://gitlab.com/porrametict/face_recognition_classic_blue)