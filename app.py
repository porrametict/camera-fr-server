from app import  app
import time 
import  requests
import threading

HOST = "0.0.0.0"
PORT =8000 
class AutoClusteringUnknownFace(threading.Thread): 
    def __init__(self) :
        threading.Thread.__init__(self)
    
    def run(self) : 
        hour  = 1 
        time_sleep = hour * 60 *60 
        url = f"http://{HOST}:{PORT}/api/face/clustering"
        fail_couter = 0 

        while  True :
            try : 
                requests.post(url)
            except :
                print("except")
                fail_couter += 1 
            if fail_couter > 10 :
                break 
            time.sleep(time_sleep)
                

auto_clustering = AutoClusteringUnknownFace() 
if __name__ == "__main__" :
    auto_clustering.start()
    app.run(debug=True,host=HOST,port=PORT)